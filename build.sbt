import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.5",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "Hello",
    scalaVersion := "2.12.2",
    mainClass in (Compile, run) := Some("example.Main"),
    mainClass in assembly := Some("example.Main"),
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-dsl" % "0.18.10",
      "org.http4s" %% "http4s-blaze-server" % "0.18.10",
      "org.http4s" %% "http4s-blaze-client" % "0.18.10",
      "org.http4s" %% "http4s-circe" % "0.18.10",
      "io.circe" %% "circe-generic" % "0.6.1",
      scalaTest % Test
    ) 
  )
