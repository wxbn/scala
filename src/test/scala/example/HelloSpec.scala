package example

import org.scalatest._

class HelloSpec extends FlatSpec with Matchers {

  val a1 = Airport("id", "ident", "ftype", "name",
                    "latitude_deg",
                    "longitude_deg",
                    "elevation_ft",
                    "continent",
                    "iso_country",
                    "iso_region",
                    "municipality",
                    "scheduled_service",
                    "gps_code",
                    "iata_code",
                    "local_code",
                    "home_link",
                    "wikipedia_link",
                    "keywords")

  "Aiport id" should "be its id" in {
      a1.id shouldEqual "id"
  }

  "Aiport gps_code" should "be its gps_code" in {
      a1.gps_code shouldEqual "gps_code"
  }

  "Aiport keywords" should "be its keywords" in {
      a1.keywords shouldEqual "keywords"
  }

  val c1 = Country("id",
                    "code",
                    "name",
                     "continent",
                     "wikipedia_link",
                     "keywords")


  "Country id" should "be its id" in {
      c1.id shouldEqual "id"
  }

  "Country code" should "be its code" in {
      c1.code shouldEqual "code"
  }

  "Country name" should "be its name" in {
      c1.name shouldEqual "name"
  }

  "Country keywords" should "be its keywords" in {
      c1.keywords shouldEqual "keywords"
  }

   val r1 = RunWay("id",
  "airport_ref",
  "airport_ident",
  "length_ft",
  "width_ft",
  "surface",
  "lighted",
  "closed",
  "le_ident",
  "le_latitude_deg",
  "le_longitude_deg",
  "le_elevation_ft",
  "le_heading_degT",
  "le_displaced_threshold_ft",
  "he_ident",
  "he_latitude_deg",
  "he_longitude_deg",
  "he_elevation_ft",
  "he_heading_degT",
  "he_displaced_threshold_ft")


  "RunWay id" should "be its id" in {
      r1.id shouldEqual "id"
  }
  
  "RunWay surface" should "be its surface" in {
      r1.surface shouldEqual "surface"
  }

  "RunWay ident" should "be its he_ident" in {
      r1.he_ident shouldEqual "he_ident"
  }

  "CSV Parser" should "remove quotes" in {
    CSVParser.toStr("\"str\"") shouldEqual "str"
  }

  "CSV Parser" should "read all airports" in {
    DB.airports.size shouldEqual 46505
  }

  "CSV Parser" should "read all countries" in {
    DB.countries.size shouldEqual 247
  }

  "CSV Parser" should "read all runways" in {
    DB.runways.size shouldEqual 39536
  }

  "QueryTask " should "query correct FR" in { 
    QueryTask.jsonQuery("FR").size shouldEqual 789
  }

  "QueryTask " should "query correct NZ" in { 
    QueryTask.jsonQuery("NZ").size shouldEqual 205
  }

  "QueryTask " should "query correct US" in { 
    QueryTask.jsonQuery("US").size shouldEqual 21501
  }

  "QueryTask " should "query correct TO" in { 
    QueryTask.jsonQuery("TO").size shouldEqual 6
  }

  "QueryTask " should "query correct AE" in { 
    QueryTask.jsonQuery("AE").size shouldEqual 32
  }

  "QueryTask " should "query correct BE" in { 
    QueryTask.jsonQuery("BE").size shouldEqual 122
  }

  "QueryTask " should "query correct CH" in { 
    QueryTask.jsonQuery("CH").size shouldEqual 75
  }

  "QueryTask " should "query correct GB" in { 
    QueryTask.jsonQuery("GB").size shouldEqual 489
  }

  "jsonTypeRun " should "have correct number for FR" in { 
    ReportsTask.jsonTypeRun("FR").size shouldEqual 22
  }

  "jsonTypeRun " should "have correct number for US" in { 
    ReportsTask.jsonTypeRun("US").size shouldEqual 168
  }

  "jsonTypeRun " should "have correct number for GB" in { 
    ReportsTask.jsonTypeRun("GB").size shouldEqual 25
  }

  "jsonTypeRun " should "have correct number for FI" in { 
    ReportsTask.jsonTypeRun("FI").size shouldEqual 17
  }

  "jsonTypeRun " should "have correct number for JP" in { 
    ReportsTask.jsonTypeRun("JP").size shouldEqual 10
  }

  "jsonTypeRun " should "have correct number for IL" in { 
    ReportsTask.jsonTypeRun("IL").size shouldEqual 3
  }

  "jsonTypeRun " should "have correct number for IT" in { 
    ReportsTask.jsonTypeRun("IT").size shouldEqual 13
  }

  "jsonTypeRun " should "have correct number for TH" in { 
    ReportsTask.jsonTypeRun("TH").size shouldEqual 2
  }

  "jsonTypeRun " should "have correct number for AU" in { 
    ReportsTask.jsonTypeRun("AU").size shouldEqual 45
  }

  "jsonTypeRun " should "have correct number for BR" in { 
    ReportsTask.jsonTypeRun("BR").size shouldEqual 34
  }

  "jsonTypeRun " should "have correct number for NA" in { 
    ReportsTask.jsonTypeRun("NA").size shouldEqual 4
  }





}
