package example

import cats.effect._, org.http4s._, org.http4s.dsl.io._, scala.concurrent.ExecutionContext.Implicits.global

import fs2.{Stream, StreamApp}
import fs2.StreamApp.ExitCode
import org.http4s.server.blaze._

import org.http4s.circe._
import io.circe._, io.circe.generic.auto._, io.circe.syntax._

object Server extends StreamApp[IO] {
    val service = HttpService[IO] {
        case GET -> Root / "query" / query => Ok(QueryTask.jsonQuery(query).asJson)
        case GET -> Root / "report" / "top10" => Ok(ReportsTask.jsonTop10.asJson)
        case GET -> Root / "report" / "low10" => Ok(ReportsTask.jsonLow10.asJson)
        case GET -> Root / "report" / "typeRunaway" / query => Ok(ReportsTask.jsonTypeRun(query).asJson)
        case GET -> Root / "report" / "commonLat" => Ok(ReportsTask.jsonRunLat.asJson)
    }

    override def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, ExitCode] =
        BlazeBuilder[IO]
            .bindHttp(8080, "localhost")
            .mountService(service, "/")
            .serve
}