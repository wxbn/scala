package example

import scala.io

case class Airport(
                    id:String,
                    ident:String,
                    ftype:String,
                    name:String,
                    latitude_deg:String,
                    longitude_deg:String,
                    elevation_ft:String,
                    continent:String,
                    iso_country:String,
                    iso_region:String,
                    municipality:String,
                    scheduled_service:String,
                    gps_code:String,
                    iata_code:String,
                    local_code:String,
                    home_link:String,
                    wikipedia_link:String,
                    keywords:String
                  )
{

  def dump: Unit = {
    println(s"ident=$ident, type=$ftype, name=$name, lat=$latitude_deg, long=$longitude_deg, continent=$continent")
  }

}

case class Country (
                     id:String,
                     code:String,
                     name:String,
                     continent:String,
                     wikipedia_link:String,
                     keywords:String
                   )
{

  def dump: Unit = {
    println(s"code=$code, name=$name, continent=$continent, wiki=$wikipedia_link, keywords=$keywords")
  }

}

case class RunWay (
  id:String,
  airport_ref:String,
  airport_ident:String,
  length_ft:String,
  width_ft:String,
  surface:String,
  lighted:String,
  closed:String,
  le_ident:String,
  le_latitude_deg:String,
  le_longitude_deg:String,
  le_elevation_ft:String,
  le_heading_degT:String,
  le_displaced_threshold_ft:String,
  he_ident:String,
  he_latitude_deg:String,
  he_longitude_deg:String,
  he_elevation_ft:String,
  he_heading_degT: String,
  he_displaced_threshold_ft:String
)
{
  def dump: Unit = {
    println(s"ref=$airport_ref, ident=$airport_ident, length=$length_ft, width=$width_ft, surface=$surface")
  }
}

object CSVParser {

  def toStr(s: String) = s match {
    case "" => ""
    case x => x.drop(1).dropRight(1)
  }


  def read_airports() : List[Airport] = {
    io.Source.fromResource ("data/airports.csv").getLines().drop(1).map (line => {
      val tmp = line.split(",", -1).map(_.trim)

      val cols = tmp.foldLeft(List.empty[String]) {
        case(olds::l, s) if l.length > 0 && olds.startsWith("\"") && !olds.endsWith("\"") => (olds.concat(s))::l
        case(l, s) => s::l
      }.reverse

      Airport(cols(0), toStr(cols(1)), toStr(cols(2)), toStr(cols(3)), cols(4), cols(5), cols(6), toStr(cols(7)), toStr(cols(8)), toStr(cols(9)),
        toStr(cols(10)), toStr(cols(11)), toStr(cols(12)), toStr(cols(13)), toStr(cols(14)), toStr(cols(15)), toStr(cols(16)), toStr(cols(17)))
  }).toList
  }

  def read_countries() : List[Country] = {
    io.Source.fromResource ("data/countries.csv").getLines().drop(1).map (line => {
      val cols = line.split(",", -1).map(_.trim)
      Country(cols(0), toStr(cols(1)), toStr(cols(2)), toStr(cols(3)), toStr(cols(4)), toStr(cols(5)))
    }).toList
  }

  def read_runways() : List[RunWay] = {
    io.Source.fromResource ("data/runways.csv").getLines().drop(1).map (line => {
      val cols = line.split(",", -1).map(_.trim)
      RunWay(cols(0), cols(1), toStr(cols(2)), cols(3), cols(4), toStr(cols(5)), cols(6), cols(7), toStr(cols(8)), toStr(cols(9)),
        toStr(cols(10)), toStr(cols(11)), toStr(cols(12)), toStr(cols(13)), toStr(cols(14)), toStr(cols(15)), toStr(cols(16)), toStr(cols(17)), toStr(cols(18)), toStr(cols(19)))
    }).toList
  }


}


object DB {

  val airports : List[Airport] = CSVParser.read_airports()
  val countries : List[Country] = CSVParser.read_countries()
  val runways : List[RunWay] = CSVParser.read_runways()

}


object QueryTask {

  def jsonQuery(name: String) = {
    val code = DB.countries.find(p => p.name == name || p.code == name) match {
      case Some(c) => c.code
      case None => ""
    }

    DB.airports.filter(a => a.iso_country == code)
  }

  def query(name: String) = {
    val code = DB.countries.find(p => p.name == name || p.code == name) match {
      case Some(c) => c.code
      case None => ""
    }

    DB.airports.filter(a => a.iso_country == code).foreach(a => {
      a.dump
      println("Airways:")
      DB.runways.filter(x => x.airport_ref == a.id).foreach(x => x.dump)
      println()
    })
  }

  def run(): Unit = {
    print("Country name or code:")
    val name =  scala.io.StdIn.readLine()
    query(name)
  }

}

object ReportsTask {

case class countryCount(count: Int, country: Country)

  def airports_map = DB.airports.foldLeft(Map.empty[String, Int]) {
    case(map, airport) if map.contains(airport.iso_country) =>  map + (airport.iso_country -> (map(airport.iso_country) + 1))
    case(map, airport) => map + (airport.iso_country -> 1)
  }

  def jsonTop10() = {
    def generate_top10 = airports_map.toList.sortBy(c => c._2).reverse.take(10)
    generate_top10.map(cc => (DB.countries.find(c => c.code == cc._1).get, cc._2)).map{
      case (country, count) => countryCount(count, country)
    }
  }

  def jsonLow10() = {
    def generate_low10 = airports_map.toList.sortBy(c => c._2).take(10)
    generate_low10.map(cc => (DB.countries.find(c => c.code == cc._1).get, cc._2)).map{
      case (country, count) => countryCount(count, country)
    }
  }

  def hi10() = {
    def generate_top10 = airports_map.toList.sortBy(c => c._2).reverse.take(10)
    generate_top10.map(cc => (DB.countries.find(c => c.code == cc._1).get, cc._2)).zipWithIndex.foreach { case ((country, count), pos) => {
        print(pos + 1)
        print(". (")
        print(count)
        print(") ")
        country.dump
    }}
  }

  def low10() = {
    def generate_low10 = airports_map.toList.sortBy(c => c._2).take(10)
    generate_low10.map(cc => (DB.countries.find(c => c.code == cc._1).get, cc._2)).zipWithIndex.foreach { case ((country, count), pos) => {
        print(pos + 1)
        print(". (")
        print(count)
        print(") ")
        country.dump
    }}
  }

  def jsonTypeRun(name: String) = {
    val code = DB.countries.find(p => p.name == name || p.code == name) match {
      case Some(c) => c.code
      case None => ""
    }

    val airports = DB.airports.filter(a => a.iso_country == code).map(x => x.id)
    DB.runways.filter(x => airports.contains(x.airport_ref)).map(x => x.surface).distinct
  }

  def type_run(): Unit = {
    print("Country name or code: ")
    val name =  scala.io.StdIn.readLine()

    val code = DB.countries.find(p => p.name == name || p.code == name) match {
      case Some(c) => c.code
      case None => ""
    }

    println("Types of runways:")
    val airports = DB.airports.filter(a => a.iso_country == code).map(x => x.id)
    DB.runways.filter(x => airports.contains(x.airport_ref)).map(x => x.surface).distinct.foreach(println)
  }

  case class latCount(count: Int, le_ident: String)

  def jsonRunLat() = {
    DB.runways.groupBy(x => x.le_ident).toList.sortBy(x => x._2.size).reverse.take(10).zipWithIndex.map {case (x, pos) => {
      latCount(x._2.size, x._1)
    }}
  }

  def run_lat(): Unit = {
    DB.runways.groupBy(x => x.le_ident).toList.sortBy(x => x._2.size).reverse.take(10).zipWithIndex.foreach {case (x, pos) => {
      print(pos + 1)
      print(". (")
      print(x._2.size)
      print(") ")
      println(x._1)
    }}
  }

  def run(): Unit = {
    println("What report do yo want ? ")
    println("1. Top 10 countries with highest number of airports")
    println("2. Top 10 countries with lowest number of airports")
    println("3. Type of runaways per countries")
    println("4. Top 10 most common runway latitude")
    scala.io.StdIn.readLine() match {
      case "1" => hi10()
      case "2" => low10()
      case "3" => type_run()
      case "4" => run_lat()
      case _ => {
        println("Invaid choice, try again")
        run
      }
    }
  }

}

object Interface {

  def chooseTask : Unit = {
    println("Choose an action:")
    println("1) Query")
    println("2) Reports")
    scala.io.StdIn.readLine() match {
      case "1" => QueryTask.run()
      case "2" => ReportsTask.run()
      case _ => {
        println("Invaid choice, try again")
        chooseTask
      }
    }
  }

  def main(args: Array[String]) {
    chooseTask
  }
}
