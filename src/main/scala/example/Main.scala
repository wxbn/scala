package example

object Main {
    def main(args: Array[String]) {
        println("Choose how to launch the application :")
        println("[D] default interface")
        println("[S] server")
        scala.io.StdIn.readLine() match {
            case "S" => example.Server.main(Array.empty)
            case "s" => example.Server.main(Array.empty)
            case _ => example.Interface.main(Array.empty)
        }
    }
}